import "./styles/global.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from "./pages/home/Home";
import MovieDetail from "./pages/detail/MovieDetail";
import SerieDetail from "./pages/detail/SerieDetail";
import Authpage from "./pages/auth/Authpage";
import Movies from "./pages/movies/Movies";
import Series from "./pages/series/Series";
import Profile from "./pages/profile/Profile";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/movies/:id",
    element: <MovieDetail />,
  },

  {
    path: "/series/:id",
    element: <SerieDetail />,
  },
  {
    path: "/auth",
    element: <Authpage />,
  },
  {
    path: "/series",
    element: <Series />,
  },
  {
    path: "/movies",
    element: <Movies />,
  },
  {
    path: "/profile",
    element: <Profile />,
  },
]);

function App() {
  return (
    <div className="App">
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
