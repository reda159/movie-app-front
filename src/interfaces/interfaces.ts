export interface IContent {
  id: number;
  name: string;
  type: string;
  movieDbId: number
}

export interface IUser {
  id: number;
  name: string;
  password: string;
}

export interface IWatch {
  id_content: number;
  id_user: number;
  status: string;
  content: IContent
}

export interface IAppreciate {
  id_content: number;
  id_user: number;
  rating: string;
  notice: string;
}

export interface IMovieAPI {
  id: number;
  title: string;
  poster_path: string;
  overview: string;
  release_date: Date;
  vote_average: number;
  genres: IGenreAPI[];
}

export interface ISerieAPI {
  id: number;
  name: string;
  poster_path: string;
  overview: string;
  first_air_date: Date;
  vote_average: number;
  genres: IGenreAPI[];
  number_of_seasons: number;
}

export interface ISearchMovieAPI {
  id: number;
  title: string;
  poster_path: string;
  overview: string;
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  original_language: string;
  original_title: string;
  video: boolean;
  popularity: number;
  release_date: Date;
  vote_average: number;
  vote_count: number;
  media_type: string;
}

export interface ISearchSerieAPI {
  id: number;
  name: string;
  poster_path: string;
  overview: string;
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  original_language: string;
  original_name: string;
  first_air_date: Date;
  popularity: number;
  media_type: string;
  vote_average: number;
  vote_count: number;
  origin_country: string[];
}

export interface IGenreAPI {
  id: number;
  name: string;
}
