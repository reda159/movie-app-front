import React, { useEffect, useRef, useState } from "react";
import { Typography, ThemeProvider, Container } from "@mui/material";
import Card from "@mui/material/Card";
import { IMovieAPI, ISerieAPI, IWatch } from "../../interfaces/interfaces";
import useAxios from "../../hooks/Global/useAxios";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import MovieCard from "../../components/MovieCard";
import SerieCard from "../../components/SerieCard";
import theme from "../../theme/theme";
import Topbar from "../../components/Topbar";
import Bottombar from "../../components/Bottombar";
import { useSelector } from "react-redux";
import { RootState } from "../../store/store";

const Profile: React.FunctionComponent = () => {
  const { fetchData } = useAxios();
  const dataFetch = useRef<boolean>(false);
  const [lMovies, setLMovies] = useState<Array<IMovieAPI>>([]);
  const [lSeries, setLSeries] = useState<Array<ISerieAPI>>([]);
  const userState = useSelector((state: RootState) => state.user);

  useEffect(() => {
    if (dataFetch.current) {
      return;
    }
    dataFetch.current = true;
    fetchData({
      method: "GET",
      url: `/watch?userId=${userState.id}`,
      headers: {
        accept: "*/*",
      },
    }).then((response) => {
      const lMoviesTemp: Array<IMovieAPI> = [];
      const lSeriesTemp: Array<ISerieAPI> = [];
      response?.data.content.forEach((item: IWatch) => {
        if (item.content.type === "MOVIE") {
          fetchData(
            {
              method: "GET",
              url: `/movie/${item.content.movieDbId}`,
              headers: {
                accept: "*/*",
              },
            },
            false,
            "movie_id"
          ).then((response) => {
            lMoviesTemp.push(response?.data);
            setLMovies(lMoviesTemp);
          });
        } else if (item.content.type === "SERIES") {
          fetchData(
            {
              method: "GET",
              url: `/tv/${item.content.movieDbId}`,
              headers: {
                accept: "*/*",
              },
            },
            false,
            "serie_id"
          ).then((response) => {
            lSeriesTemp.push(response?.data);
            setLSeries(lSeriesTemp);
          });
        }
      });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <ThemeProvider theme={theme}>
      <Container>
        <Topbar />
        <Typography sx={{textAlign: "center", my: 2}} variant="h3">Profil</Typography>
        <Typography variant="body1">
          <div>
            <h1>Films aimés</h1>
            <div
              style={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: "start",
              }}
            >
              {lMovies.map((movie: IMovieAPI) => (
                <Card sx={{ maxWidth: 180, m: 2, px: 1, py: 2 }} key={movie.id}>
                  <MovieCard movie={movie} />
                </Card>
              ))}
            </div>
            <h1>Series aimés</h1>
            <div
              style={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: "start",
              }}
            >
              {lSeries.map((serie: ISerieAPI) => (
                <Card sx={{ maxWidth: 180, m: 2, px: 1, py: 2 }} key={serie.id}>
                  <SerieCard serie={serie} />
                </Card>
              ))}
            </div>
          </div>
        </Typography>
        <Bottombar />
      </Container>
    </ThemeProvider>
  );
};
export default Profile;
