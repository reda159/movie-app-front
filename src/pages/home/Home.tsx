import { useEffect, useState } from "react";
import { Container, Typography } from "@mui/material";
import Topbar from "../../components/Topbar";
import Bottombar from "../../components/Bottombar";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../../theme/theme";
import useAxios from "../../hooks/Global/useAxios";
import { IMovieAPI, ISerieAPI } from "../../interfaces/interfaces";
import MovieCard from "../../components/MovieCard";
import SerieCard from "../../components/SerieCard";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Home: React.FunctionComponent = () => {

  const settings = {
    dots: false,
    infinite: true,
    arrow: true,
    speed: 250,
    slidesToShow: 8,
    slidesToScroll: 8,
    responsive: [
      {
        breakpoint: 1600,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 6,
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const [lMovies, setLMovies] = useState<Array<IMovieAPI>>([]);
  const [lSeries, setLSeries] = useState<Array<ISerieAPI>>([]);

  const { fetchData } = useAxios();

  useEffect(() => {
    fetchData(
      {
        method: "GET",
        url: `/movie/popular`,
        headers: {
          accept: "*/*",
        },
      },
      false
    ).then((response) => {
      setLMovies(response?.data.results);
    });

    fetchData(
      {
        method: "GET",
        url: `/tv/popular`,
        headers: {
          accept: "*/*",
        },
      },
      false
    ).then((response) => {
      setLSeries(response?.data.results);
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <Topbar/>
        <Typography variant="body1">
          <div>
            <h1>Films</h1>
            <Slider {...settings}>
              {lMovies.map((movie: IMovieAPI) => (
                <MovieCard key={movie.id} movie={movie} />
              ))}
            </Slider>
            <h1>Series</h1>
            <Slider {...settings}>
              {lSeries.map((serie: ISerieAPI) => (
                <SerieCard key={serie.id} serie={serie} />
              ))}
            </Slider>
          </div>
        </Typography>
        <Bottombar />
      </Container>
    </ThemeProvider>
  );
};

export default Home;
