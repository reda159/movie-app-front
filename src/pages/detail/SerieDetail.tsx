import { useEffect, useState } from "react";
import { Card, Chip, Container, Grid, Typography } from "@mui/material";
import Topbar from "../../components/Topbar";
import Bottombar from "../../components/Bottombar";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../../theme/theme";
import useAxios from "../../hooks/Global/useAxios";
import { ISerieAPI } from "../../interfaces/interfaces";
import { useParams } from "react-router-dom";
import CardMedia from "@mui/material/CardMedia";
import { Box } from "@mui/material";

const SerieDetail: React.FunctionComponent = () => {
  const [serie, setSerie] = useState<ISerieAPI>();
  const { id } = useParams<{ id: string }>();

  const { fetchData } = useAxios();
  useEffect(() => {
    fetchData(
      {
        method: "GET",
        url: `/tv/${id}`, // Use the id parameter in the URL
        headers: {
          accept: "*/*",
        },
      },
      false
    ).then((response) => {
      setSerie(response?.data);
      console.log(response?.data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <Topbar />
        <Grid sx={{ my: 4 }} container spacing={3} className="detail">
          <Typography variant="body1">
            <div>
              {serie !== undefined && (
                <Box sx={{ width: 700 }} key={serie.id} className="movieDetail">
                  <Card
                    sx={{
                      backgroundColor: "#1262ac30",
                      border: "2px solid #1262ac",
                      p: 4,
                    }}
                    variant="outlined"
                  >
                    <Typography
                      sx={{ textAlign: "center", mb: 1 }}
                      variant="h4"
                    >
                      {serie.name}
                    </Typography>
                    <Typography variant="h6"></Typography>
                    {serie.genres.map((genre) => (
                      <Chip
                        color="primary"
                        sx={{ m: 1 }}
                        label={genre.name}
                      ></Chip>
                    ))}
                    <CardMedia
                      sx={{ margin: "auto", my: 3 }}
                      component="img"
                      src={`https://image.tmdb.org/t/p/w500${serie.poster_path}`}
                      title={serie.name}
                    />
                    <Typography
                      sx={{ textAlign: "center", mb: 4 }}
                      variant="h5"
                    >
                      <strong>Note :</strong> {serie.vote_average}/10
                    </Typography>
                    <Typography
                      sx={{ textAlign: "center", mb: 4 }}
                      variant="h5"
                    >
                      {serie.number_of_seasons > 1 ? (
                        <strong>{serie.number_of_seasons} saisons</strong>
                      ) : (
                        <strong>{serie.number_of_seasons} saison</strong>
                      )}
                    </Typography>
                    <Typography sx={{ textAlign: "justify" }} variant="h6">
                      {serie.overview}
                    </Typography>
                    <Typography sx={{ mt: 2 }} variant="h6">
                      <strong>Date de sortie :</strong>{" "}
                      {serie.first_air_date.toLocaleString()}
                    </Typography>
                  </Card>
                </Box>
              )}
            </div>
          </Typography>
        </Grid>
        <Bottombar />
      </Container>
    </ThemeProvider>
  );
};

export default SerieDetail;
