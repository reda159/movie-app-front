import { useEffect, useState } from "react";
import { Card, Chip, Container, Grid, Typography } from "@mui/material";
import Topbar from "../../components/Topbar";
import Bottombar from "../../components/Bottombar";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../../theme/theme";
import useAxios from "../../hooks/Global/useAxios";
import { IMovieAPI } from "../../interfaces/interfaces";
import { useParams } from "react-router-dom";
import CardMedia from "@mui/material/CardMedia";
import { Box } from "@mui/material";

const MovieDetail: React.FunctionComponent = () => {
  const [movie, setMovie] = useState<IMovieAPI>();
  const { id } = useParams<{ id: string }>();

  const { fetchData } = useAxios();
  useEffect(() => {
    fetchData(
      {
        method: "GET",
        url: `/movie/${id}`, // Use the id parameter in the URL
        headers: {
          accept: "*/*",
        },
      },
      false
    ).then((response) => {
      setMovie(response?.data);
      console.log(response?.data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <Topbar />
        <Grid sx={{ my: 4 }} container spacing={3} className="detail">
          <Typography variant="body1">
            <div>
              {movie !== undefined && (
                <Box sx={{ width: 700 }} key={movie.id} className="movieDetail">
                  <Card
                    sx={{
                      backgroundColor: "#1262ac30",
                      border: "2px solid #1262ac",
                      p: 4,
                    }}
                    variant="outlined"
                  >
                    <Typography
                      sx={{ textAlign: "center", mb: 1 }}
                      variant="h4"
                    >
                      {movie.title}
                    </Typography>
                    <Typography variant="h6"></Typography>
                    {movie.genres.map((genre) => (
                      <Chip
                        color="primary"
                        sx={{ m: 1 }}
                        label={genre.name}
                      ></Chip>
                    ))}

                    <CardMedia
                      sx={{ margin: "auto", my: 3 }}
                      component="img"
                      src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
                      title={movie.title}
                    />
                    <Typography
                      sx={{ textAlign: "center", mb: 4 }}
                      variant="h5"
                    >
                      <strong>Note :</strong> {movie.vote_average}/10
                    </Typography>
                    <Typography sx={{ textAlign: "justify" }} variant="h6">
                      {movie.overview}
                    </Typography>
                    <Typography sx={{ mt: 2 }} variant="h6">
                      <strong>Date de sortie :</strong>{" "}
                      {movie.release_date.toLocaleString()}
                    </Typography>
                  </Card>
                </Box>
              )}
            </div>
          </Typography>
        </Grid>
        <Bottombar />
      </Container>
    </ThemeProvider>
  );
};

export default MovieDetail;
