import { useEffect, useState } from "react";
import {
  Card,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Pagination,
  Select,
  Typography,
} from "@mui/material";
import Topbar from "../../components/Topbar";
import Bottombar from "../../components/Bottombar";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../../theme/theme";
import useAxios from "../../hooks/Global/useAxios";
import { IMovieAPI, IGenreAPI } from "../../interfaces/interfaces";
import MovieCard from "../../components/MovieCard";

const Movies: React.FunctionComponent = () => {
  const [lMovies, setLMovies] = useState<IMovieAPI[]>([]);

  const [sortBy, setSortBy] = useState<string>("popular");

  const [genres, setGenres] = useState<IGenreAPI[]>([]);

  const [genre, setGenre] = useState<any>();

  const [page, setPage] = useState<number>(1);

  const [count, setCount] = useState<number>();

  const { fetchData } = useAxios();

  useEffect(() => {
    fetchData(
      {
        method: "GET",
        url: "/genre/movie/list",
        headers: {
          accept: "*/*",
        },
      },
      false
    ).then((response) => {
      setGenres(response?.data.genres);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    let sortByDiscover = "popularity.desc";
    if (sortBy === "top_rated") {
      sortByDiscover = "vote_average.desc";
    } else if (sortBy === "incoming") {
      sortByDiscover = "first_air_date.desc";
    }
    let query = `&page=${page}&sort_by=${sortByDiscover}`;
    if (genre) {
      query = `&with_genres=${genre}&page=${page}&sort_by=${sortByDiscover}`;
    }
    fetchData(
      {
        method: "GET",
        url: "/discover/movie",
        headers: {
          accept: "*/*",
        },
      },
      false,
      query
    ).then((response) => {
      setLMovies(response?.data.results);
      if (response?.data.total_pages <= 500) {
        setCount(response?.data.total_pages);
      } else {
        setCount(500);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sortBy, genre, page]);

  const changePage = (e: any, value: number) => {
    setPage(value);
  };

  const changeGenre = (e: any) => {
    setGenre(e.target.value);
  };

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <Topbar />
        <Typography
          sx={{ mt: 2, textAlign: "center", textDecoration: "underline" }}
          variant="h3"
        >
          Films
        </Typography>
        <FormControl sx={{ m: 2 }}>
          <InputLabel id="genre-label">Genre</InputLabel>
          <Select
            labelId="genre-label"
            value={genre}
            label="Genre"
            onChange={changeGenre}
            autoWidth
            sx={{ minWidth: 100 }}
          >
            {genres.map((gen: IGenreAPI) => {
              return (
                <MenuItem key={gen.id} value={gen.id}>
                  {gen.name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <FormControl sx={{ m: 2 }}>
          <InputLabel id="sortby-label">Trier par</InputLabel>
          <Select
            labelId="sortby-label"
            value={sortBy}
            label="Trier par"
            onChange={(e) => setSortBy(e.target.value)}
            autoWidth
          >
            <MenuItem value="popular">Les plus populaires</MenuItem>
            <MenuItem value="top_rated">Les mieux notés</MenuItem>
            <MenuItem value="upcoming">En ce moment</MenuItem>
          </Select>
        </FormControl>
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          {lMovies.map((movie: IMovieAPI) => (
            <Card sx={{ maxWidth: 180, m: 2, px: 1, py: 2 }} key={movie.id}>
              <MovieCard movie={movie} />
            </Card>
          ))}
        </div>
        <Pagination
          sx={{ display: "flex", justifyContent: "center", my: 2 }}
          count={count}
          onChange={changePage}
          color="primary"
        />
        <Bottombar />
      </Container>
    </ThemeProvider>
  );
};

export default Movies;
