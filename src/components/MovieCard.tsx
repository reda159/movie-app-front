import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { Typography, Box, CardMedia, Grid } from "@mui/material";
import { IMovieAPI } from "../interfaces/interfaces";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import FavoriteIcon from "@mui/icons-material/Favorite";
import useAxios from "../hooks/Global/useAxios";
import { useSelector } from "react-redux";
import { RootState } from "../store/store";

interface Params {
  movie: IMovieAPI;
}

const MovieCard: React.FC<Params> = ({ movie }) => {
  const [movieLiked, setMovieLiked] = useState(false);
  const userState = useSelector((state: RootState) => state.user);
  const { fetchData } = useAxios();
  useEffect(() => {
    fetchData({
      method: "GET",
      url: `/watch/${userState.id}/${movie.id}`,
      headers: {
        accept: "*/*",
      },
    })
      .then((response) => {
        console.log(userState);
        if (response !== undefined) {
          setMovieLiked(true);
        }
      })
      .catch((error) => setMovieLiked(false));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const setToSee = () => {
    if (!movieLiked) {
      setMovieLiked(true);
      fetchData({
        method: "POST",
        data: {
          content: {
            name: movie.title,
            type: "MOVIE",
            movieDbId: movie.id,
          },
          user: {
            id: userState.id,
          },
          status: "TO_SEE",
        },
        url: `/watch`,
        headers: {
          accept: "*/*",
        },
      }).then((response) => {
        if (response?.status === 200) {
          setMovieLiked(response.data);
        }
      });
    } else {
      setMovieLiked(false);
      fetchData({
        method: "DELETE",
        url: `/watch/1/${movie.id}`,
        headers: {
          accept: "*/*",
        },
      });
    }
  };

  return (
    <Box key={movie.id} className="carousel-item">
      <Link
        style={{ color: "initial", textDecoration: "none" }}
        to={`/movies/${movie.id}`}
      >
        <CardMedia
          component="img"
          src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
          title={movie.title}
        />
      </Link>

      <Grid container>
        <Grid item xs={10}>
          <Typography variant="h6">{movie.title}</Typography>
        </Grid>
        <Grid item xs={2}>
          {movieLiked ? (
            <FavoriteIcon
              sx={{ cursor: "pointer" }}
              onClick={setToSee}
              className="liked"
            ></FavoriteIcon>
          ) : (
            <FavoriteBorderIcon
              sx={{ cursor: "pointer" }}
              onClick={setToSee}
            ></FavoriteBorderIcon>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};

export default MovieCard;
