import {
  AppBar,
  Divider,
  Toolbar,
  Typography,
  InputBase,
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@mui/material";
import { ThemeProvider, styled, alpha } from "@mui/material/styles";
import theme from "../theme/theme";
import SearchIcon from "@mui/icons-material/Search";
import { useNavigate } from "react-router";
import { useState } from "react";
import { ISearchMovieAPI, ISearchSerieAPI } from "../interfaces/interfaces";
import useAxios from "../hooks/Global/useAxios";
import { Link } from "react-router-dom";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

function Topbar(props: any) {
  const navigate = useNavigate();

  const { fetchData } = useAxios();

  const [searches, setSearches] = useState<
    ISearchMovieAPI[] | ISearchSerieAPI[]
  >();

  const [searchValue, setSearchValue] = useState<string>("");

  const goToRoute = (route: string) => {
    navigate(route);
  };

  const handleSearchChange = (event: any) => {
    const value = event.target.value;
    setSearchValue(value);
    fetchData(
      {
        method: "GET",
        url: `/search/multi`,
        headers: {
          accept: "*/*",
        },
      },
      false,
      `&query=${value}`
    ).then((response: any) => {
      const searches = response.data.results.slice(0, 6);
      setSearches(searches);
    });
  };

  const blurSearch = (event: any) => {
    setTimeout(() => {
      setSearches([]);
    }, 200);
  };

  return (
    <ThemeProvider theme={theme}>
      <AppBar color="primary" position="static">
        <Toolbar>
          <img
            width="150px"
            src="/img/logo.png"
            alt="logo movie app"
            style={{ cursor: "pointer", marginTop: "4px" }}
            onClick={() => goToRoute("/")}
          />
          <Divider
            variant="middle"
            orientation="vertical"
            flexItem
            sx={{ bgcolor: "primary.contrastText" }}
          />
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, cursor: "pointer", textAlign: "center" }}
            onClick={() => goToRoute("/profile")}
          >
            Profil
          </Typography>
          <Divider
            variant="middle"
            orientation="vertical"
            flexItem
            sx={{ bgcolor: "primary.contrastText" }}
          />
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, cursor: "pointer", textAlign: "center" }}
            onClick={() => goToRoute("/movies")}
          >
            Films
          </Typography>
          <Divider
            variant="middle"
            orientation="vertical"
            flexItem
            sx={{ bgcolor: "primary.contrastText" }}
          />
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, cursor: "pointer", textAlign: "center" }}
            onClick={() => goToRoute("/series")}
          >
            Series
          </Typography>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              key="searchField"
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
              value={searchValue}
              onChange={handleSearchChange}
              onBlur={blurSearch}
            />
          </Search>
        </Toolbar>
      </AppBar>
      {searches && searches.length > 0 ? (
        <List
          sx={{
            width: "100%",
            maxWidth: 360,
            float: "right",
            border: "2px solid #1262AC",
            borderRadius: "0 0 8px 8px",
          }}
        >
          {searches.map((search) => {
            let movie: ISearchMovieAPI | null = null;
            let serie: ISearchSerieAPI | null = null;
            if (search.media_type === "movie") {
              movie = search as ISearchMovieAPI;
            } else {
              serie = search as ISearchSerieAPI;
            }
            return movie ? (
              <div key={movie.id}>
                <Link
                  style={{ color: "initial", textDecoration: "none" }}
                  to={`/movies/${movie.id}`}
                >
                  <ListItem alignItems="center">
                    <ListItemAvatar>
                      <Avatar
                        src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
                      />
                    </ListItemAvatar>
                    <ListItemText primary={movie.title} />
                  </ListItem>
                </Link>
                <Divider variant="middle" />
              </div>
            ) : serie ? (
              <div key={serie.id}>
                <Link
                  style={{ color: "initial", textDecoration: "none" }}
                  to={`/series/${serie.id}`}
                >
                  <ListItem alignItems="center">
                    <ListItemAvatar>
                      <Avatar
                        src={`https://image.tmdb.org/t/p/w500${serie.poster_path}`}
                      />
                    </ListItemAvatar>
                    <ListItemText primary={serie.name} />
                  </ListItem>
                </Link>
                <Divider variant="middle" />
              </div>
            ) : null;
          })}
        </List>
      ) : null}
    </ThemeProvider>
  );
}

export default Topbar;
