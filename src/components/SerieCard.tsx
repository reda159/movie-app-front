import React, { useEffect, useState } from "react";
import { Box, CardMedia, Grid, Typography } from "@mui/material";
import { ISerieAPI } from "../interfaces/interfaces";
import useAxios from "../hooks/Global/useAxios";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { RootState } from "../store/store";

interface Props {
  serie: ISerieAPI;
}

const SerieCard: React.FC<Props> = ({ serie }) => {
  const [serieLiked, setSerieLiked] = useState(false);
  const { fetchData } = useAxios();
  const userState = useSelector((state: RootState) => state.user);


  useEffect(() => {
    fetchData({
      method: "GET",
      url: `/watch/${userState.id}/${serie.id}`,
      headers: {
        accept: "*/*",
      },
    })
      .then((response) => {
        if (response !== undefined) {
          setSerieLiked(true);
        }
      })
      .catch((error) => setSerieLiked(false));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const setToSee = () => {
    if (!serieLiked) {
      setSerieLiked(true);
      fetchData({
        method: "POST",
        data: {
          content: {
            name: serie.name,
            type: "SERIES",
            movieDbId: serie.id,
          },
          user: {
            id: userState.id,
          },
          status: "TO_SEE",
        },
        url: `/watch`,
        headers: {
          accept: "*/*",
        },
      }).then((response) => {
        if (response?.status === 200) {
          setSerieLiked(response.data);
        }
      });
    } else {
      setSerieLiked(false);
      fetchData({
        method: "DELETE",
        url: `/watch/1/${serie.id}`,
        headers: {
          accept: "*/*",
        },
      });
    }
  };

  return (
    <Box key={serie.id} className="carousel-item">
      <Link
        style={{ color: "initial", textDecoration: "none" }}
        to={`/series/${serie.id}`}
      >
        <CardMedia
          component="img"
          src={`https://image.tmdb.org/t/p/w500${serie.poster_path}`}
          title={serie.name}
        />
      </Link>

      <Grid item xs={10}>
        <Typography variant="h6">{serie.name}</Typography>
      </Grid>
      <Grid item xs={2}>
        {serieLiked ? (
          <FavoriteIcon
            sx={{ cursor: "pointer" }}
            onClick={setToSee}
            className="liked"
          ></FavoriteIcon>
        ) : (
          <FavoriteBorderIcon
            sx={{ cursor: "pointer" }}
            onClick={setToSee}
          ></FavoriteBorderIcon>
        )}
      </Grid>
    </Box>
  );
};
export default SerieCard;
