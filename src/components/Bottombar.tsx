import { AppBar, Divider, Toolbar, Typography } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import theme from "../theme/theme";
import LogoutIcon from "@mui/icons-material/Logout";
import { useDispatch } from "react-redux";
import { logoutUser } from "../store/slices/UserSlice";
import { useNavigate } from "react-router-dom";

function Bottombar() {
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const logout = () => {
    dispatch(logoutUser());
    navigate("/auth");
  };

  return (
    <ThemeProvider theme={theme}>
      <AppBar color="primary" position="static" sx={{ top: "auto", bottom: 0 }}>
        <Toolbar sx={{ display: "flex", justifyContent: "center" }}>
          <Typography variant="h6" component="div">
            © 2023 Movie app
          </Typography>
          <Divider
            variant="middle"
            orientation="vertical"
            flexItem
            sx={{ bgcolor: "primary.contrastText", mx: 3 }}
          />
          <LogoutIcon sx={{ cursor: "pointer" }} onClick={logout} />
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
}

export default Bottombar;
