# Movie app

MovieApp permet aux utilisateurs de gérer une liste de films et de séries à regarder, de noter les films et les séries qu'ils ont déjà vus, de connaître les prochains films qui sortent, de choisir un film à regarder de manière aléatoire en fonction de son genre, et de gérer leur propre compte utilisateur.