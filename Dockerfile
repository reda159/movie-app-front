FROM node:18-alpine as movie_app_front
WORKDIR /front
COPY . /front
RUN npm install
CMD ["npm","start"]
EXPOSE 3000